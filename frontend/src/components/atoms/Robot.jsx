import React from 'react'
import { useSelector, useDispatch } from 'react-redux'

import "./Robot.css"
import { Alert, AlertTitle } from '@material-ui/lab';

import { setVisibleRobots_redux, setSelectedRobots_redux} from '../../redux/actions'
import {formatDate, deletedDublicates_array} from "../../services/calc"


const validation_robotsMax = (selectedRobotsToValidate) => {

    let arrayIDs = selectedRobotsToValidate.map(selectedRobot => selectedRobot.name);

    const amountOfRobots = deletedDublicates_array(arrayIDs ? arrayIDs : []).length
    
    return !( amountOfRobots > 5 )
}



const Robot = ({ robot, index, isCart }) => {
    const dispatch = useDispatch()
    const selectedRobots = useSelector(state => state.robots.selectedRobots)
    const visibleRobots = useSelector(state => state.robots.visibleRobots)

    const [isAlert, setAlert] = React.useState(false)
    let selectedRobotDisabled = false

    //remove alert for new list
    React.useEffect(()=>{
        setAlert(false)
        selectedRobotDisabled = true
    },[robot])

    const addToCart = () => {
        console.log('asdf')
        setAlert(false) 
        if( validation_robotsMax([{...robot}, ...selectedRobots]) ) {
            //add in Redux store the Robot item for "Cart"
            dispatch(setSelectedRobots_redux([{...robot}, ...selectedRobots]))

            //decrease "Stock" for chosen Robot to render relevant amount of the Robot Market
            dispatch(setVisibleRobots_redux(visibleRobots.map(visibleRobot => {
                if(visibleRobot.name === robot.name) visibleRobot.stock = visibleRobot.stock - 1 
                return visibleRobot
            })))
        } else 
            setAlert(true)    
    }

    const oneMoreRobotValidation = () => {
        //const addedSelectedRobot = selectedRobots.filter(element => robot.name === element.name)[0]
        //addedSelectedRobot && console.log(visibleRobots.filter(vis => vis.id === addedSelectedRobot.id)[0].id)
        return true
    }

    const deleteFromCart = () => {
        //delete in Redux store the Robot item from "Cart"
        dispatch(setSelectedRobots_redux(selectedRobots.filter(selectedRobot => selectedRobot !== selectedRobots[index])))

        //increase "Stock" for chosen Robot to render relevant amount of the Robot Market
        dispatch(setVisibleRobots_redux(visibleRobots.map(visibleRobot => {
            if(visibleRobot.name === robot.name) visibleRobot.stock = visibleRobot.stock + 1 
            return visibleRobot
        })))
    }


    
    return (
        <div className={"robot"}>
            <img src={robot.image}/>
            <span>Name: {robot.name}</span>
            <span>Price: ฿{robot.price}</span>
            {!isCart && <span>Stock: {robot.stock}</span>}
            <span>Created date: {formatDate(robot.createdAt)}</span>
            <span>Material: {robot.material}</span>
            <span>Material: {robot.material}</span>
            
            {
                isCart ? <>
                        <button 
                            onClick={addToCart}
                            className={"delete_cart"}
                            disabled={selectedRobotDisabled}
                            >
                                One more this Robot
                        </button>     
                        <button 
                            onClick={deleteFromCart}
                            className={"delete_cart"}>
                                Delete from Cart
                        </button>
                    </>
                    :
                    <button 
                        onClick={addToCart} 
                        disabled={robot.stock === 0}
                        className={robot.stock === 0 ? "add_cart disabled" : "add_cart"}>
                            Add to Cart
                    </button>
            }
            {isAlert && (
                <Alert severity="error">
                    <AlertTitle>User can add up to 5 different robots to cart, but they can select as much as they want in the same type until it runs out of stoc</AlertTitle>
                    if user try to add <strong>more that 5 different robots</strong> then it should show an alert
                </Alert>
                )
            }
        </div>
    )
}
export default Robot